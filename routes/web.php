
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/

Route::get('/', 'HomeController@index');

Route::get('/{groupFilter}', 'HomeController@index');

Auth::routes();

Route::get('/feeds', 'FeedController@index');



Route::post('/feed/new/', 'FeedController@create');

Route::group(array('prefix' => 'feed'), function()
{
    Route::post('/group/new/', 'FeedGroupController@create');
    
    Route::delete('group/{feedGroup}', 'FeedGroupController@delete');
    
});

Route::post('/feed/{feed}', 'FeedController@edit');

Route::get('/feed/{feed}', 'FeedController@view');

Route::get('/changePassword/request/', 'Auth\PasswordController@edit');

Route::post('/changePassword', 'Auth\PasswordController@change');

Route::delete('/feed/{feed}', 'FeedController@delete');

Route::group(array('prefix' => 'get'), function()
{
    Route::get('feed/{itemId}', function ($itemId) {
        $feedItem = App\FeedItems::find($itemId);
        if (empty($feedItem)) {
            $status = 404;
        } else {
            $status = 200;
        }
        return \Response::json($feedItem, $status);
    });
});
