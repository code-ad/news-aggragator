 @extends('layouts.app')
 @section('content')

<div class="panel-body">
	<ul class="feed-groups nav nav-pills">
		<li 
			@if (empty($active_feed_group_id)) class="active" 
			@endif 
			><a class="feed-group" href="{{url('/')}}">all</a></li>
		@foreach ($groups as $group)
			<li @if ($active_feed_group_id === $group->id) class="active" @endif ><a class="feed-group" href="{{url('/' . $group->id )}}" data-id="{{ $group->id }}">{{ $group->name }}</a></li> 
		@endforeach
	</ul>

	{{-- Feeds --}}
	@if (count($feeds) > 0)
	<div class="panel panel-default">
		<div class="panel-heading">Current feeds</div>

		<div class="panel-body">
			@inject('feedItems', 'App\FeedItems')

			<ul class="feed-channel">
				@foreach ($feeds as $feed)
				<li><a href="{{ $feed->provider_url }}" target="_blank">{{ $feed->title }}</a></li>
				<ul class="feed-items">
					@foreach ($feed['feedItems'] as $feedItem)
						<li>
							@if (!empty($feedItem->pubDate))
							{{ \Carbon\Carbon::createFromTimeStamp(strtotime($feedItem->pubDate))->diffForHumans() }}
							@endif
							<a class="feed-open" href="#" data-id="{{ $feedItem->id }}">
								
								{{ $feedItem->title }}
							</a>
						</li> 
					@endforeach
				</ul>
				@endforeach
			</ul>
		</div>
	</div>
	@else <i>No updated feeds to show</i> @endif

	<!-- Modal -->
	<div class="modal fade" id="feed-modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Feed details</h4>
				</div>
				<div class="modal-body">
					<h4 class="feed-title"></h4>
					<div class="feed-content"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a target="_blank" class="btn btn-primary visit-feed-url">Go to feed URL</a>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
