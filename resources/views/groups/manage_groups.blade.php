{{-- New Group Form --}}
	<form action="{{ url('feed/group/new') }}" method="POST" class="form-horizontal">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="task" class="col-sm-3 control-label">Group name</label>

			<div class="col-sm-6">
				<input type="text" id="feed-url"  name="name" value="{{old('url')}}" class="form-control">
			</div>
		</div>

		<!-- Add Task Button -->
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-default">
					<i class="fa fa-plus"></i> Add group
				</button>
			</div>
		</div>
	</form>

	<!-- Current Feeds -->
	@if (count($groups) > 0)
	<div class="panel panel-default">
		<div class="panel-heading">Groups</div>

		<div class="panel-body">
			<table class="table table-striped groups-table">

				<!-- Table Headings -->
				<thead>
					<th>Group name</th>
				</thead>

				<!-- Table Body -->
				<tbody>
					@foreach ($groups as $group)
					<tr>
						<td>{{ $group->name }}</td>
						<td>
							<form action="{{ url('feed/group/'.$group->id) }}" method="POST">
								{!! csrf_field() !!} {!! method_field('DELETE') !!}

								<button data-confirm-text="Do you want to delete this group?" class="confirm btn-primary">Delete group</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@endif