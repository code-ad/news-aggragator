 @extends('layouts.app') @section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
	<!-- Display Validation Errors -->
	@include('common.errors')

	<ul id="admin-tabs" class="nav nav-pills">
  		<li role="presentation" class="feeds"><a href="#feeds">Feeds</a></li>
  		<li role="presentation" class="groups"><a href="#groups">Feed groups</a></li>
	</ul>
	<div id="feeds" style="display: none;" class="tab">
		@include('feeds.manage_feeds')
	</div>
	<div id="groups" style="display: none;" class="tab">
		@include('groups.manage_groups')
	</div>
</div>
@endsection
