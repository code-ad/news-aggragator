{{-- New Feed Form --}}
	<form action="{{ url('feed/new') }}" method="POST" class="form-horizontal">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="task" class="col-sm-3 control-label">URL</label>

			<div class="col-sm-6">
				<input type="text" id="feed-url"  name="url" value="{{old('url')}}" class="form-control">
			</div>
		</div>

		<!-- Add Task Button -->
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-default">
					<i class="fa fa-plus"></i> Add feed
				</button>
			</div>
		</div>
	</form>

	<!-- Current Feeds -->
	@if (count($feeds) > 0)
	<div class="panel panel-default">
		<div class="panel-heading">Current feeds</div>

		<div class="panel-body">
			<table class="table table-striped feed-table">

				<!-- Table Headings -->
				<thead>
					<th>URL</th>
					<th>Group</th>
				</thead>

				<!-- Table Body -->
				<tbody>
					@foreach ($feeds as $feed)
					<tr>
						<td><a href="{{ url('feed/'.$feed->id) }}">{{ $feed->url }}</a></td>
						<!-- Delete Button -->
						<td>
							{{$feed['groupName']}}
						</td>
						<td>
							<form action="{{ url('feed/'.$feed->id) }}" method="POST">
								{!! csrf_field() !!} {!! method_field('DELETE') !!}

								<button data-confirm-text="Do you want to delete this url?" class="confirm btn-primary">Delete feed</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@endif