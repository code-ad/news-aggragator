@extends('layouts.app')

@section('content')
	<!-- Display Validation Errors -->
	@include('common.errors')
	<form action="{{ url('feed', $feed->id) }}" method="POST" class="form-horizontal">
		<input type="hidden" id="feed-id"  name="id" value="{{$feed->id}}">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="feed-url" class="col-sm-3 control-label">URL</label>

			<div class="col-sm-6">
				<input type="text" id="feed-url"  name="url" value="{{$feed->url}}" class="form-control">
			</div>
		</div>
			<div class="form-group">
			<label for="feed-url" class="col-sm-3 control-label">group name</label>
			<div class="col-sm-6">
				<select id="feed-group-id"  name="feed_group_id" class="form-control">
				<option value="">Unassigned</option>
				@foreach($groups as $group)
				<option value="{{ $group->id }}">{{ $group->name}}</option>
				@endforeach
				</select>
				
			</div>
			
			
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-6">
				<button type="submit" class="btn btn-default">
					<i class="fa fa-plus"></i> Update
				</button>
			</div>
		</div>

	</form>

@endsection