
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$('document').ready(function() {;
	
	$('.feed-open').click(function(e) {
		var $dialog = $('#feed-modal');
		$dialog.modal('show');
		$.ajax({
			url:'/get/feed/' + $(this).data('id'),
			type:'GET',
			success: function(x) {
				$dialog.find('.modal-body .feed-title').html(x.title);
				$dialog.find('.modal-body .feed-content').html(x.description);
				$dialog.find('.modal-footer .visit-feed-url').attr('href', x.url);
			},
			error: function(x) {
		}
		});
	
		e.preventDefault();
	});
});