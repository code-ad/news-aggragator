
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

function switch_tab(tabName)
{	
	$('.tab').hide();
	
	$('#admin-tabs li').removeClass('active');
	if (tabName == '') {
		
		tabName = 'feeds';
	}
	console.log(tabName);
	$('#' + tabName).show();
	$('#admin-tabs li.' + tabName).addClass('active');

}

$('document').ready(function() {;
	
	var hash = window.location.hash;
	switch_tab(hash.replace('#', ''));
	
	$('#admin-tabs a').click(function(e) {
		switch_tab($(this).attr('href').replace('#', ''));
		
	});
	
	$('.confirm').click(function() {
		return confirm($(this).data('confirm-text'));
	});
	
	
});
