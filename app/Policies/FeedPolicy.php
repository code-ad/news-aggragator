<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class FeedPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function destroy(User $user)
    {
        return (bool)$user->id;
    }
    
    public function view(User $user)
    {
        return (bool)$user->id;
    
    }
}
