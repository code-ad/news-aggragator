<?php
namespace App\Contracts;

interface FeedReader
{
    function read($url);
    function getHttpCode();
    function getErrorCode();
    function getErrorMessage();
    function isValidFeed();
    function isEmptyFeed();
}