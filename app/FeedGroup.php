<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedGroup extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function feeds()
    {
        return $this->hasMany(Feed::class);
    }
}

