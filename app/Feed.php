<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = ['title', 'url', 'feed_group_id', 'last_updated'];

    public function feedItems()
    {
        return $this->hasMany(FeedItems::class);
    }
}

