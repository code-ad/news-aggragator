<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use App\FeedItems;
use App\Contracts\FeedReader;

class FeedUpdate extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all registered feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Feed $feed, FeedReader $feedReader)
    {
        if (! function_exists('curl_init')) {
            $this->error('cURL is missing!');
            return false;
        }
        $feeds = $feed->orderBy('last_updated', 'asc')->get();
        $feedsTotal = count($feeds);
        
        $this->info($feedsTotal . ' feeds to update.');
        if ($feedsTotal == 0) {
            $this->info('No feeds to update');
            return true;
        }
        $this->output->progressStart($feedsTotal);
        foreach ($feeds as $feedRecord) {
            
            $this->info('Updating ' . $feedRecord->url . '...');
            
            $feedXml = $feedReader->read($feedRecord->url);
            
            if ($feedReader->getHttpCode() === 200) {
                try {
                    if (! $feedReader->isValidFeed()) {
                        $this->warn('Invalid feed');
                        $this->output->progressAdvance();
                        continue;
                    }
                    
                    if ($feedReader->isEmptyFeed()) {
                        $this->warn('Empty feed');
                        $this->output->progressAdvance();
                        continue;
                    }
                    
                    $mainFeed = $feed->find($feedRecord->id);
                    $mainFeed->feedItems()
                        ->where('feed_id', $feedRecord->id)
                        ->delete();
                    
                    foreach ($feedXml->channel->item as $data) {
                        
                        if (! isset($data->description) && ! isset($data->title)) {
                            $this->warn('skipping empty entry');
                            continue;
                        }
                        $itemTitle = isset($data->title) ? (string) $data->title : null;
                        $itemDescription = isset($data->description) ? (string) $data->description : null;
                        $itemPubDate = ! empty($data->pubDate) ? (string) $data->pubDate : null;
                        $itemLink = ! empty($data->link) ? (string) $data->link : null;
                        // deletes old feed entries
                        
                        // inserting new feed entry
                        $mainFeed->feedItems()->create([
                            'title' => $itemTitle,
                            'description' => $itemDescription,
                            'url' => $itemLink,
                            'pubDate' => $itemPubDate
                        ]);
                    }
                    $mainFeed->last_updated = \Carbon\Carbon::now()->toDateTimeString();
                    $mainFeed->title = $feedXml->channel->title;
                    $mainFeed->provider_url = $feedXml->channel->link;
                    
                    $mainFeed->save();
                } catch (\Exception $e) {
                    // $this->warn($e->getMessage());
                }
            } else {
                $this->warn("Error has occured while updating: {$feedReader->getErrorMessage()}. Http code received: {$feedReader->getHttpCode()}");
            }
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        
        return true;
    }
}
