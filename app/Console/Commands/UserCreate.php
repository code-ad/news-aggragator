<?php
namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UserCreate extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $username = $this->argument('username');
        $existingUser = User::where('username', $username)->get()->first();
        if ($existingUser) {
            $this->error('Such username is already exists!');
            return false;
        }
        
        $password = $this->secret(
            'Password for ' . $username . ' user:');
        
        $newUser = User::create([
            'username' => $this->argument('username'),
            'password' => bcrypt($password)
        ]);
        $this->info("user {$username} (ID {$newUser->id}) has been created. ");
    }
}
