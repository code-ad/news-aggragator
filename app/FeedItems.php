<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedItems extends Model
{
    
    public $timestamps = false;
    
    protected $fillable = ['title', 'description', 'url', 'pubDate'];
    
    function feed()
    {
        return $this->belongsTo(Feed::class);
    }
    
    public function getFeedItems($feedId)
    {
        return $this->where('feed_id', $feedId)->get();
    }

}

