<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;

class PasswordController extends Controller
{

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(Request $request, User $user)
    {
        $this->authorize('changePassword', $user);
        
        return view('auth.password', [
            'user' => $user
        ]);
    }

    public function change(Request $request, User $user)
    {
        $this->authorize('changePassword', $user);
        
        $this->validate($request, [
            'password' => 'required|min:1|max:255'
        ]);
        
        $user = \App\User::find(Auth::user()->id);
        $user->password = bcrypt($request->password);
        $user->save();
        
        $request->session()->flash('alert-success', 'Password changed');
        
        return redirect('/feeds');
    }
}
