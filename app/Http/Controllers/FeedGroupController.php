<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\FeedGroup;

class FeedGroupController extends Controller
{

    public function create( Request $request, FeedGroup $feedGroup)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:255'
        ]);
        
        if ($validator->fails()) {
            return redirect('/feeds#groups')
            ->withErrors($validator)
            ->withInput();
        }
        
        $feedGroup->create([
            'name' => $request->name
        ]);
        
        return redirect('/feeds#groups');
    }
    
    public function delete(Request $request, FeedGroup $feedGroup)
    {
        $this->authorize('destroy', $feedGroup);
    
        ;
        if ($feedGroup->feeds()->get()->isEmpty()) { 

            $feedGroup->delete();
            $request->session()->flash('alert-success', 'Group was deleted successfully!');
        } else {
            $request->session()->flash('alert-danger', 'Cannot delete. There are feeds assigned to ' . $feedGroup->name .' group');
        }
    
        return redirect('/feeds#groups');
    }
}
