<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use App\FeedItems;
use App\FeedGroup;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, FeedItems $feedItems, FeedGroup $feedGroup)
    {
        $feed_group_id = $feedGroup->id;
        
        $groups = $feedGroup->all();
        $feeds = Feed::orderBy('last_updated', 'desc')->where('feed_group_id', $feed_group_id)
            ->whereNotNull('last_updated')
            ->get();
        
        foreach ($feeds as & $feed) {
            $feed['feedItems'] = $feedItems->getFeedItems($feed['id']);
            
        }
        
        return view('/home', [
            'feeds' => $feeds,
            'groups' => $groups,
            'active_feed_group_id' => $feed_group_id
        ]);
    }
}
