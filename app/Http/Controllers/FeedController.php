<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feed;
use App\FeedGroup;

class FeedController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Feed $feed, Request $request, FeedGroup $feedGroup)
    {
        $this->authorize('view', $feed);
        $groups = $feedGroup->all();
        return view('feeds.details', [
            'feed' => $feed,
            'groups' => $groups,
        ]);
    }
    
    public function getFeedDetails(Feed $feed, Request $request)
    {
        $this->authorize('view', $feed);
        return json_encode($feed);
    }

    public function index(Request $request, FeedGroup $feedGroup)
    {
        $user = $request->user();
        if (! $user) {
            
            return redirect('/');
        }
        
        $feeds = Feed::orderBy('last_updated', 'desc')->get();
        $groups = $feedGroup->all();
        
        foreach ($feeds as & $feed) {
            $group = $feedGroup->find($feed['feed_group_id']);
            if (!empty($group)) {
            $feed['groupName'] = $group->name;
            }
        }
        return view('feeds.index', [
            'feeds' => $feeds,
            'groups' => $groups,
        ]);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|active_url|max:255'
        ]);
        
        Feed::create([
            'url' => $request->url
        ]);
        $request->session()->flash('alert-success', 'Feed url was added successfully!');
        return redirect('/feeds#feeds');
    }
    
    public function edit(Request $request, Feed $feed)
    {
        $this->validate($request, [
            'url' => 'required|active_url|max:255',
            'feed_group_id' => 'integer',
        ],[
            'feed_group_id.integer' => 'group name must be valid string'
        ]);
    
        if (empty($request->feed_group_id)) {
            $request->feed_group_id = null;
        }
        
        $feed->update([
            'url' => $request->url,
            'feed_group_id' => $request->feed_group_id
        ]);
    
        return redirect('/feeds#feeds');
    }

    public function delete(Feed $feed)
    {
        $this->authorize('destroy', $feed);
        
        $feed->delete();
        
        return redirect('/feeds#feeds');
    }
}
