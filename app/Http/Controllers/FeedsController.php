<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeedItems;

class FeedsController extends Controller
{
    public function get(Request $request)
    {
        $feedItemId = $request->input('item');
        return \Response::json(FeedItems::find($feedItemId));
    }
}
