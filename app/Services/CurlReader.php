<?php
namespace App\Services;

use App\Contracts\FeedReader;
use \LibXMLError;
use \SimpleXMLElement;

class CurlReader implements FeedReader
{

    private $curlHandle;

    private $errorCode;

    private $errorMsg;

    private $httpCode;

    private $lastXmlError;

    private $feedXml;

    public function read($url)
    {
        $this->curlHandle = curl_init($url);
        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlHandle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curlHandle, CURLOPT_HEADER, false);
        curl_setopt($this->curlHandle, CURLOPT_TIMEOUT_MS, 2000);
        $response = curl_exec($this->curlHandle);
        
        $this->errorCode = curl_errno($this->curlHandle);
        $this->errorMsg = curl_error($this->curlHandle);
        $this->httpCode = curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
        
        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $this->feedXml = simplexml_load_string($response);
        $this->lastXmlError = libxml_get_last_error();
        
        if ($this->lastXmlError instanceof LibXMLError) {
            $this->errorMsg = $this->lastXmlError->message;
            $this->errorCode = $this->lastXmlError->code;
        }
        
        return $this->feedXml;
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessage()
    {
        return $this->errorMsg;
    }

    public function isValidFeed()
    {
        
        if ($this->lastXmlError instanceof LibXMLError) {
            return false;
        }
        if (! isset($this->feedXml->channel) || ! $this->feedXml->channel instanceof SimpleXMLElement) {
            return false;
        }
        return true;
    }

    public function isEmptyFeed()
    {
        if (! isset($this->feedXml->channel->item) || ! $this->feedXml->channel->item instanceof SimpleXMLElement) {
            return true;
        }
        return false;
    }
}