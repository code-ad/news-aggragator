<?php

use Illuminate\Database\Seeder;

class FeedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feeds')->delete();
        factory(App\Feed::class)->create([
           'url' => 'http://www.delfi.lt/rss'
        ]);
        
        factory(App\Feed::class)->create([
            'url' => 'http://www.delfi.lt/rss/feeds/education.xml'
        ]);
        
        factory(App\Feed::class)->create([
            'url' => 'http://kaunas.kasvyksta.lt/feed'
        ]);
        
        factory(App\Feed::class)->create([
            'url' => 'http://elektronika.lt/rss/visas'
        ]);
        
    }
}
